import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import * as fromCompanies from './stores/companies/companies.reducer';
import { CompaniesEffects } from './stores/companies/companies.effects';
import { CompaniesService } from './companies.service';

import { AppComponent } from './app.component';
import { GraphicComponent } from './components/graphic/graphic.component';
import { ViewerComponent } from './components/graphic/viewer/viewer.component';
import { CategorySelectComponent } from './components/category-select/category-select.component';

@NgModule({
  declarations: [
    AppComponent,
    GraphicComponent,
    ViewerComponent,
    CategorySelectComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    StoreModule.forRoot({companies: fromCompanies.CompaniesReducer}),
    EffectsModule.forRoot([CompaniesEffects])
  ],
  providers: [
    CompaniesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
