import { Injectable } from '@angular/core';
// import { HttpClient } from '@angular/common/http';
// import { environment } from '../environments/environment';
import { CompanyModel } from './models';
import { Observable, of } from 'rxjs';
import { companies } from './companies.mock.data';

@Injectable()
export class CompaniesService {
  // ApiURL = environment.ApiURL;

  constructor(/*private httpClient: HttpClient*/) {}

  getCompanies(): Observable<CompanyModel[]> {
    // получение данных через API
    // this.httpClient
    //   .get<CompanyModel[]>(`${this.ApiURL}/companies`)

    // mocking
    return of(companies);
  }
}
