import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import * as CompaniesActions from './companies.actions';
import { CompanyModel } from '../../models';
import {CompaniesService} from '../../companies.service';

@Injectable()
export class CompaniesEffects {
  GetCompanies$: Observable<Action> = createEffect(() =>
    this.action$.pipe(
      ofType(CompaniesActions.GetCompanies),
      mergeMap(() => this.companiesService.getCompanies()
        .pipe(
          map((data: CompanyModel[]) => {
            return CompaniesActions.SuccessGetCompanies({ companies: data });
          }),
          catchError((error: Error) => {
            return of(CompaniesActions.ErrorGetCompanies({error}));
          })
        )
      )
    )
  );

  constructor(
    private companiesService: CompaniesService,
    private action$: Actions
  ) {}
}
