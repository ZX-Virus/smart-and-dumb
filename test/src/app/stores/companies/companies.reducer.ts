import { createReducer, on } from '@ngrx/store';
import * as CompaniesActions from './companies.actions';
import CompaniesState, {initialState} from './companies.state';

export const scoreboardFeatureKey = 'companies';

const companiesReducer = createReducer(
  initialState,
  on(CompaniesActions.GetCompanies, state => state),
  on(CompaniesActions.SuccessGetCompanies, (state: CompaniesState, { companies }) => {
    return {
      ...state,
      Companies: companies,
      CompaniesError: null
    };
  }),
  on(CompaniesActions.ErrorGetCompanies, (state: CompaniesState, { error }) => {
    console.log(error);
    return {
      ...state,
      CompaniesError: error
    };
  })
);

export function CompaniesReducer(state, action) {
  return companiesReducer(state, action);
}
