import { createAction, props } from '@ngrx/store';
import { CompanyModel } from '../../models';

export const GetCompanies = createAction(
  '[Companies] Get'
);
export const SuccessGetCompanies = createAction(
  '[Companies] - Success Get',
  props<{ companies: CompanyModel[] }>()
);
export const ErrorGetCompanies = createAction(
  '[Companies] - Error Get',
  props<{ error: Error }>()
);
