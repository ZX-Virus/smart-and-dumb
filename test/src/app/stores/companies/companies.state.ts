import {CompanyModel} from '../../models';

export default class CompaniesState {
  Companies: CompanyModel[];
  CompaniesError: Error;
}

export const initialState = {
  Companies: [],
  CompaniesError: null
};
