import { WeekDaysModel } from './week-days.model';

export interface CompanyModel {
  id: number;
  name: string;
  type: string;
  revenuePerWeek: WeekDaysModel;
  revenue: number;
  monthRevenue: number;
}
