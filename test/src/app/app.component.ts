import { Component, OnInit } from '@angular/core';
import { CompaniesService } from './companies.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  constructor(private companiesService: CompaniesService) {
  }

  ngOnInit(): void {
    this.companiesService.getCompanies();
  }
}
