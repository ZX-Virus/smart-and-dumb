import {
  AfterViewInit,
  ChangeDetectionStrategy, ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import {WeekDaysModel} from '../../../models';

export interface ViewerCompany {
  id: number;
  name: string;
  category: string;
  weekStats: WeekDaysModel;
  balance: number;
  monthBalance: number;
}

interface Day {
  shortName: string;
  value: number;
}

export interface ViewerCompanyExt extends ViewerCompany {
  days: Day[];
  color: string;
}

interface Point {
  X: number;
  Y: number;
}

@Component({
  selector: 'app-graphic-viewer',
  templateUrl: './viewer.component.html',
  styleUrls: ['./viewer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ViewerComponent implements OnChanges, AfterViewInit {
  @Input() items: ViewerCompany[] = [];
  @Output() onItemSelected: EventEmitter<number> = new EventEmitter<number>();

  intItems: ViewerCompanyExt[] = [];
  showItems: ViewerCompanyExt[] = [];

  categories: string[] = [];
  private currentCategory = 'all';
  categoryItems: string[] = [];
  private currentCategoryItem: ViewerCompanyExt;
  private currentCategoryItemString = 'all';
  monthBalance: number;
  balance: number;

  @ViewChild('canvas', {read: ElementRef, static: true})
  private canvasRef: ElementRef;
  private canvas: any;
  private context2D: CanvasRenderingContext2D;
  private step: number;
  private textBlockHeight = 16;
  private colors = ['#67C7FB', 'red', 'green'];
  private maxY: number;

  constructor(private cd: ChangeDetectorRef) {
  }

  ngAfterViewInit(): void {
    this.canvas = this.canvasRef.nativeElement;
    this.context2D = this.canvas.getContext('2d');
    if (this.context2D) {
      this.context2D.canvas.width = this.canvas.clientWidth;
      this.context2D.canvas.height = this.canvas.clientHeight;
      this.context2D.font = '14px Arial';

      this.step = (this.context2D.canvas.width - 11) / 6;
      this.context2D.lineWidth = 1;
      if (this.intItems) {
        this.drawGraphicCompanies();
      }
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.items && this.intItems) {
      this.intItems = this.calculateInternalCompanies(this.items);
      this.maxY = Math.max(...this.intItems.map(company => {
        return Math.max(...company.days.map(day => day.value));
      }, 0));
      const uniqueArray = (array) => Array.from(new Set<string>(array));
      this.categories = uniqueArray(this.intItems.map(item => item.category));
      this.calculateShowItems();
      this.calcBalances();
      this.drawGraphicCompanies();
    }
  }

  calculateShowItems() {
    this.showItems = this.intItems.filter(company => {
      return (this.currentCategory === 'all') || (company.category === this.currentCategory);
    });
    this.categoryItems = this.showItems.map(company => `${company.name} (id:${company.id})`);
  }

  calculateInternalCompanies(companies: ViewerCompany[]): ViewerCompanyExt[] {
    return companies.map((company, index) => {
      const daysWeek: Day[] = [];
      const weekStats = company.weekStats;
      daysWeek.push({shortName: 'M', value: weekStats.monday});
      daysWeek.push({shortName: 'T', value: weekStats.tuesday});
      daysWeek.push({shortName: 'W', value: weekStats.wednesday});
      daysWeek.push({shortName: 'T', value: weekStats.thursday});
      daysWeek.push({shortName: 'F', value: weekStats.friday});
      daysWeek.push({shortName: 'S', value: weekStats.saturday});
      daysWeek.push({shortName: 'S', value: weekStats.sunday});
      return {
        ...company,
        days: daysWeek,
        color: this.colors[index] ? this.colors[index] : `#${(0x808080 + Math.trunc(Math.random() * 0x808080 - 1)).toString(16)}`,
      };
    });
  }

  changeCategory(value) {
    this.currentCategory = value;
    this.currentCategoryItemString = 'all';
    this.currentCategoryItem = null;
    this.calculateShowItems();
    this.calcBalances();
    this.drawGraphicCompanies();
  }

  changeCategoryItems(value) {
    this.currentCategoryItemString = value;
    const parts = value.split('id:');
    let id: number;
    if (parts.length > 1) {
      id = parseInt(parts[1].split(')')[0], 10);
    }
    this.currentCategoryItem = id ? this.showItems.find(company => company.id === id) : null;
    this.drawGraphicCompanies();
  }

  btnClick() {
    this.onItemSelected.emit(this.currentCategoryItem.id);
  }

  private calcBalances() {
    this.monthBalance = this.showItems.reduce((a, b) => a + b.monthBalance, 0);
    this.balance = this.showItems.reduce((a, b) => a + b.balance, 0);
  }

  private drawDayLines() {
    this.context2D.strokeStyle = '#F2F3F5';
    this.context2D.clearRect(0, 0, this.context2D.canvas.width, this.context2D.canvas.height);

    for (let i = 0; i < 7; i++) {
      this.context2D.beginPath();
      const x = Math.trunc(5 + i * this.step);
      this.context2D.moveTo(x, 0);
      this.context2D.lineTo(x, this.context2D.canvas.height - this.textBlockHeight);
      this.context2D.stroke();
    }
  }

  private drawGraphicCompanies() {
    if (this.context2D) {
      this.drawDayLines();

      this.intItems.forEach(company => {
        const condition =  (this.currentCategory === 'all') || (company.category === this.currentCategory);
        if (condition) {
          this.drawGraphicCompany(company, this.maxY);
        }
      });
    }
  }

  private drawGraphicCompany(company: ViewerCompanyExt, maxY: number) {
    if (this.context2D) {
      const height = this.context2D.canvas.height - this.textBlockHeight - 5;
      const kY = height / maxY;

      this.context2D.lineWidth = company === this.currentCategoryItem ? 3 : 1.5;
      this.context2D.beginPath();
      this.context2D.strokeStyle = company.color;
      let previousPoints: Point;
      for (let i = 0; i < company.days.length; i++) {
        const day = company.days[i];
        const x = Math.trunc(5 + i * this.step);
        const y = height - day.value  * kY + 5;
        if (!i) {
          this.context2D.moveTo(x, y);
        } else {
          this.context2D.bezierCurveTo(
            (previousPoints.X + x) / 2, previousPoints.Y,
            (previousPoints.X + x) / 2, y,
            x, y);
        }
        previousPoints = {X: x, Y: y};
        this.context2D.fillStyle = '#B9B9B9';
        this.context2D.fillText(day.shortName, x - 5, this.context2D.canvas.height - 2);
      }
      this.context2D.stroke();
      this.context2D.lineWidth = 1;

      // градиент рисуется грязно
      const gradientStart = this.context2D.createLinearGradient(0, 0, this.step, 0);
      gradientStart.addColorStop(0, 'rgba(255, 255, 255, .5)');
      gradientStart.addColorStop(1, 'rgba(255, 255, 255, 0)');
      this.context2D.fillStyle = gradientStart;
      this.context2D.fillRect(0, 0, this.step, height + 6);

      const gradientEnd = this.context2D.createLinearGradient(this.context2D.canvas.width - this.step, 0, this.context2D.canvas.width, 0);
      gradientEnd.addColorStop(0, 'rgba(255, 255, 255, 0)');
      gradientEnd.addColorStop(1, 'rgba(255, 255, 255, .5)');
      this.context2D.fillStyle = gradientEnd;
      this.context2D.fillRect(this.context2D.canvas.width - this.step, 0, this.context2D.canvas.width, height + 6);

      this.cd.detectChanges();
    }
  }
}
