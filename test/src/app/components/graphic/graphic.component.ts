import { Component, OnInit } from '@angular/core';
import { CompaniesService } from '../../companies.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ViewerCompany } from './viewer/viewer.component';
import { Store } from '@ngrx/store';
import CompaniesState from '../../stores/companies/companies.state';
import * as CompaniesActions from '../../stores/companies/companies.actions';

interface MappedCompany {
  companies: ViewerCompany[];
  error: Error;
}

@Component({
  selector: 'app-graphic',
  templateUrl: './graphic.component.html',
  styleUrls: ['./graphic.component.scss']
})
export class GraphicComponent implements OnInit {
  mappedCompanies$: Observable<MappedCompany>;

  constructor(private companiesService: CompaniesService,
              private store: Store<{companies: CompaniesState}>) {
    this.mappedCompanies$ = store.select(state => state.companies).pipe(
      map(payload => {
        return {
          companies: payload.Companies.map(company => {
            return {
              id: company.id,
              name: company.name,
              category: company.type,
              weekStats: company.revenuePerWeek,
              balance: company.revenue,
              monthBalance: company.monthRevenue
            } as ViewerCompany;
          }),
          error: payload.CompaniesError
        };
      })
    );
  }

  ngOnInit(): void {
    this.store.dispatch(CompaniesActions.GetCompanies());
  }

  selectItem(id: number) {
    console.log(`Company ID: ${id}`);
  }
}
