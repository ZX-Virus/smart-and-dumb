import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-category-select',
  templateUrl: './category-select.component.html',
  styleUrls: ['./category-select.component.scss']
})
export class CategorySelectComponent {
  @Input() allValue: string;
  @Input() items: string[];
  @Output() itemSelected: EventEmitter<string> = new EventEmitter<string>();

  changeItem(value: string) {
    this.itemSelected.emit(value);
  }
}
