import { CompanyModel } from './models';

export const companies: CompanyModel[] = [
  {
    id: 123,
    name: 'Company 1',
    type: 'Category 11',
    revenuePerWeek: {
      monday: 12,
      tuesday: 5,
      wednesday: 34,
      thursday: 15,
      friday: 10,
      saturday: 21,
      sunday: 20
    },
    revenue: 1500,
    monthRevenue: 345
  },
  {
    id: 7324,
    name: 'Company 2',
    type: 'Category 11',
    revenuePerWeek: {
      monday: 44,
      tuesday: 12,
      wednesday: 33,
      thursday: 6,
      friday: 23,
      saturday: 1,
      sunday: 0
    },
    revenue: 1600,
    monthRevenue: 300
  },
  {
    id: 26,
    name: 'Company 3',
    type: 'Category 22',
    revenuePerWeek: {
      monday: 50,
      tuesday: 7,
      wednesday: 5,
      thursday: 22,
      friday: 64,
      saturday: 12,
      sunday: 3
    },
    revenue: 1000,
    monthRevenue: 400
  },
  {
    id: 2892,
    name: 'Company 4',
    type: 'Category 22',
    revenuePerWeek: {
      monday: 0,
      tuesday: 3,
      wednesday: 4,
      thursday: 21,
      friday: 43,
      saturday: 55,
      sunday: 99
    },
    revenue: 2000,
    monthRevenue: 350
  },
  {
    id: 303,
    name: 'Company 5',
    type: 'Category 22',
    revenuePerWeek: {
      monday: 32,
      tuesday: 23,
      wednesday: 12,
      thursday: 44,
      friday: 56,
      saturday: 78,
      sunday: 56
    },
    revenue: 2200,
    monthRevenue: 260
  },
  {
    id: 86,
    name: 'Company 6',
    type: 'Category 33',
    revenuePerWeek: {
      monday: 87,
      tuesday: 55,
      wednesday: 67,
      thursday: 78,
      friday: 67,
      saturday: 98,
      sunday: 87
    },
    revenue: 3000,
    monthRevenue: 500
  }
];
